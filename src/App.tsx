import './App.css';
import Cabecalho from './components/Cabecalho';
import Main from './components/Main';
import { useState } from 'react';

function App() {
  const [carrinho, setCarrinho] = useState(false);

  return (
    <>
      <Cabecalho carrinho={carrinho} setCarrinho={setCarrinho} />
      <Main carrinho={carrinho} setCarrinho={setCarrinho} />
    </>
  );

}

export default App;
