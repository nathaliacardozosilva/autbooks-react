import classNames from 'classnames';
import style from './Modal.module.scss';

interface Props {
    setModalAtivo: React.Dispatch<React.SetStateAction<boolean>>,
}

export default function Modal({ setModalAtivo }: Props) {
    return (
        <>
            <div className={style["regulamento__modal"]}>
                <h2 className={style["regulamento__titulo"]}>Quanto mais livros, mais desconto!</h2>
                <h3 className={style["regulamento__subtitulo"]}>Confira o regulamento da promoção:</h3>
                <p className={style["regulamento__texto"]}>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis 
                    nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu 
                    fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in 
                    culpa qui officia deserunt mollit anim id est laborum."</p>
                <p className={style["regulamento__texto"]}>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis 
                    nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."</p>
                <button className={style["regulamento__botao"]} onClick={() => setModalAtivo(false)}>Voltar à loja</button>
            </div>
        </>
    )
}