import style from './Carrinho.module.scss';
import botaoFechar from '../../assets/img/icones/fechar.svg'
import Botao from '../Botao';
import React, { useEffect, useState } from 'react';
import livros from '../../livros.json'
import Item from './Item';
import { ILivro } from 'components/Main';
import classNames from 'classnames';

interface Props {
	setCarrinho: React.Dispatch<React.SetStateAction<boolean>>,
	carrinho: boolean
}

function calcularDesconto(listaCarrinho: ILivro[], soma: number): number {
	let desconto: number;

	if (listaCarrinho.length == 2) {
			desconto = (soma * 0.05);
	} else if (listaCarrinho.length == 3) {
			desconto = soma * 0.1;
	} else if (listaCarrinho.length == 4) {
			desconto = soma * 0.2;
	} else if (listaCarrinho.length >= 5) {
			desconto = soma * 0.25;
	} else {
			desconto = 0;
	}

	return desconto;
}

function calcularSoma(listaCarrinho: Array<ILivro>): number {
	let soma: number = 0;

	for (const item of listaCarrinho) {
		soma += item.valorTotal;
	}

	return soma;
}

function quantidadeItens(listaCarrinho: Array<ILivro>): number {
	let quantidadeTotal: number = 0;

	for (const item of listaCarrinho) {
		quantidadeTotal += item.quantidade;
	}

	return quantidadeTotal;
}

export default function Carrinho({ setCarrinho, carrinho }: Props) {
	const [soma, setSoma] = useState<number>(0);
	const [desconto, setDesconto] = useState<number>(0);
	const [quantidadeTotal, setQuantidadeTotal] = useState<number>(0);
	const [carrinhoAtualizado, setCarrinhoAtualizado] = useState<Array<ILivro>>([]);
	
	let listaCarrinho: Array<ILivro> = [];
	
	useEffect(() => {
		setSoma(calcularSoma(listaCarrinho));
		setQuantidadeTotal(quantidadeItens(listaCarrinho));
		setDesconto(calcularDesconto(listaCarrinho, calcularSoma(listaCarrinho)));
		setCarrinhoAtualizado(listaCarrinho);
	},[listaCarrinho])

	return (
		<>
			<span className={style["carrinho__quantidade-total"]} onClick={() => setCarrinho(true)}>{quantidadeTotal}</span>
			<section className={classNames({
				[style.carrinho]: true,
				[style.carrinhoAtivo]: carrinho === true
			})}>
				<div className={style["carrinho__cabecalho"]}>
					<h2 className={style["carrinho__titulo"]}>Meu carrinho</h2>
					<img src={botaoFechar} alt="Fechar carrinho" className={style["carrinho__fechar"]} onClick={() => setCarrinho(false)} />
				</div>
				<div className={style["carrinho__container"]}>
					{
						livros.map((livro) => {
							if(livro.quantidade > 0) {
								listaCarrinho.push(livro);
								return (
									<Item 
										imagem={livro.imagem} 
										nome={livro.nome} 
										valorTotal={livro.valorTotal} 
										quantidade={livro.quantidade}
										key={livro.id}
									/>
								)
							}
						})
					}
				</div>
				<div className={style["carrinho__informacoes"]}>
					<div className={style["carrinho__textos"]}>
						<p>Subtotal</p>
						<p>Total de descontos</p>
						<p className={style["carrinho__textos__total"]}>Total</p>
					</div>
					<div className={style["carrinho__valores"]}>
						<p>R$ {soma.toFixed(2).toString().replace('.', ',')}</p>
						<p>R$ {desconto.toFixed(2).toString().replace('.', ',')}</p>
						<p className={style["carrinho__valores__total"]}>R$ {(soma - desconto).toFixed(2).toString().replace('.', ',')}</p>
					</div>
				</div>
				<Botao>
						Finalizar compra
				</Botao>
			</section>
		</>
	)
}