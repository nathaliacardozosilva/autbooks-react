import style from './Item.module.scss';
import lixeira from '../../../assets/img/icones/lixeira.svg';

interface Props {
	imagem: string;
	nome: string;
	valorTotal: number;
	quantidade: number;
}

export default function Item({imagem, nome, valorTotal, quantidade}: Props) {
	return (
		<article className={style["carrinho__produto"]}>
			<img src={imagem} alt={nome} className={style["carrinho__produto__imagem"]} />
			<div className={style["carrinho__produto__info"]}>
				<p className={style["carrinho__produto__titulo"]}>{nome}</p>
				<div className={style["carrinho__produto__valores"]}>
					<p className={style["carrinho__produto__preco"]}>R$ {valorTotal.toFixed(2).toString().replace('.', ',')}</p>
					<p className={style["carrinho__produto__quantidade"]}>{quantidade}</p>
					<img src={lixeira} className={style["carrinho__produto__excluir"]} />
				</div>
			</div>
		</article>
	)
}