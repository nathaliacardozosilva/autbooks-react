import style from './MenuCascata.module.scss';
import styles from '../../Menu.module.scss';
import logo from '../../../assets/img/logo-autbooks-sidebar.svg';

interface Props {
	setCarrinho: React.Dispatch<React.SetStateAction<boolean>>
}

export default function MenuCascata({ setCarrinho }: Props) {
	return (
		<div className={style["menu__container"]}>
			<img src={logo} alt="Logotipo da Autbooks" className={style["menu__logo"]} />
			<a href="#" className={`${style.menu__link} ${styles.menu__linkInicio} ${style.menu__linkAtivo}`}>Início</a>
			<a href="#" className={`${style.menu__link} ${styles.menu__linkConta}`}>Minha conta</a>
			<a href="#" className={`${style.menu__link} ${styles.menu__linkCarrinho}`} onClick={() => setCarrinho(true)}>Meu carrinho</a>
			<a href="#" className={`${style.menu__link} ${styles.menu__linkAjuda}`}>Ajuda</a>
			<a href="#" className={`${style.menu__link} ${styles.menu__linkPedidos}`}>Meus pedidos</a>
		</div>
	)
}