import style from './Cabecalho.module.scss';
import menu from '../../assets/img/icones/menu.svg';
import iconecarrinho from '../../assets/img/icones/carrinho-branco.svg';
import logo from '../../assets/img/logo.svg';
import botaoFechar from '../../assets/img/icones/fechar-branco.svg';
import MenuLateral from './MenuLateral';
import MenuCascata from './MenuCascata';
import Carrinho from '../Carrinho';
import classNames from 'classnames';
import { useState } from 'react';

interface Props {
    carrinho: boolean,
    setCarrinho: React.Dispatch<React.SetStateAction<boolean>>
}

export default function Cabecalho({ carrinho, setCarrinho }: Props) {
    const [menuCascataIsVisible, setMenuCascataIsVisible] = useState(false);
    const [menuLateralIsVisible, setMenuLateralIsVisible] = useState(false);

    return (
        <header className={style["cabecalho"]}>
            <div className={style["cabecalho__container"]}>
                <img src={menu} alt="Menu em cascata" className={`${style.cabecalho__menu}`} onClick={() => setMenuCascataIsVisible(!menuCascataIsVisible)} />
                <img src={menu} alt="Menu em cascata" className={`${style.cabecalho__menuMobile}`} onClick={() => setMenuLateralIsVisible(!menuCascataIsVisible)} />
                <h1>
                    <a href="#">
                        <img src={logo} alt="Logo da Autbooks" className={style["cabecalho__logo"]} />
                    </a>
                </h1>
                <img src={iconecarrinho} alt="Carrinho de compras" className={style["cabecalho__carrinho"]} onClick={() => setCarrinho(!carrinho)} />
            </div>
            <section className={classNames({
                [style.menuLateral]: true,
                [style.menuLateralAtivo]: menuLateralIsVisible === true
            })}>
			    <img src={botaoFechar} alt="Fechar menu lateral" className={`${style.menuLateral__fechar}`} onClick={() => setMenuLateralIsVisible(!menuLateralIsVisible)} />
                <MenuLateral setMenuLateralIsVisible={setMenuLateralIsVisible} setCarrinho={setCarrinho} />
            </section>
            <section className={classNames({
                [style.menu]: true,
                [style.menuAtivo]: menuCascataIsVisible === true
            })}>
                <MenuCascata setCarrinho={setCarrinho} />
            </section>
            <Carrinho setCarrinho={setCarrinho} carrinho={carrinho} />
        </header>
    );
}