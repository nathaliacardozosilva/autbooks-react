import style from './MenuLateral.module.scss';
import logoSidebar from '../../../assets/img/logo-autbooks-sidebar.svg';
import styles from '../../Menu.module.scss';

interface Props {
	setMenuLateralIsVisible: React.Dispatch<React.SetStateAction<boolean>>,
	setCarrinho: React.Dispatch<React.SetStateAction<boolean>>
}

export default function MenuLateral({ setMenuLateralIsVisible, setCarrinho }: Props) {
	return (
		<>
			<img src={logoSidebar} alt="Logo da Autbooks" className={`${style.menuLateral__logo}`} />
			<ul className={`${style.menuLateral__navegacao}`}>
				<li className={`${style.menuLateral__link} ${styles.menu__linkInicio} ${style.menuLateral__navegacaoAtivo}`}><a href="#">Início</a></li>
				<li className={`${style.menuLateral__link} ${styles.menu__linkConta}`}><a href="#">Minha conta</a></li>
				<li className={`${style.menuLateral__link} ${styles.menu__linkCarrinho}`} onClick={() => {
					setCarrinho(true)
					setMenuLateralIsVisible(false)
					}}><a href="#">Meu carrinho</a></li>
				<li className={`${style.menuLateral__link} ${styles.menu__linkAjuda}`}><a href="#">Ajuda</a></li>
				<li className={`${style.menuLateral__link} ${styles.menu__linkPedidos}`}><a href="#">Meus pedidos</a></li>
			</ul>
		</>
	)
}