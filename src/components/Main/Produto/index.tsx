import style from './Produto.module.scss';
import Botao from '../../Botao';
import livros from 'livros.json';

interface ILivro {
	id: number;
	imagem: string;
	nome: string;
	valorInicial: number;
	valorTotal: number;
	quantidade: number;
}

interface Props {
	snackbar: boolean,
	setSnackbar: React.Dispatch<React.SetStateAction<boolean>>
}

export default function Produto({ snackbar, setSnackbar }: Props) {
	return (
		<>
			{livros.map((livro) => (
				<article className={style["produtos__produto"]} key={livro.id}>
					<figure className={style["produto__figura"]}>
						<img src={livro.imagem} alt="" className={style["produto__imagem"]} />
						<figcaption className={style["produto__titulo"]}>
							{livro.nome}
						</figcaption>
					</figure>
					<p className={style["produto__preco"]}>R$ {livro.valorInicial.toFixed(2).toString().replace('.', ',')}</p>
					<Botao 
						onClick={() => {
							livro.quantidade = livro.quantidade + 1;
							livro.valorTotal = livro.quantidade * livro.valorInicial

							setSnackbar(true)
						}}
					>
						Adicionar ao carrinho
					</Botao>
				</article>
			))}
		</>
	)
}
