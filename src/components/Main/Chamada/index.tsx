import style from './Chamada.module.scss';
import livrosChamada1 from '../../../assets/img/livros-chamada-1.png';
import livrosChamada2 from '../../../assets/img/livros-chamada-2.png';
import Modal from '../../Modal';
import { useState } from 'react';
import classNames from 'classnames';

interface Props {
    modalAtivo: boolean,
    setModalAtivo: React.Dispatch<React.SetStateAction<boolean>>
}

export default function Chamada({ modalAtivo, setModalAtivo }: Props) {
    return (
        <section className={style["chamada"]}>
            <div className={style["chamada__container"]}>
                <img src={livrosChamada1} alt="Conjunto de livros dos Jogos Vorazes" className={style["chamada__imagem-1"]} />
                <img src={livrosChamada2} alt="Conjunto de livros de Harry Potter" className={style["chamada__imagem-2"]} />
                <div className={style["chamada__texto"]}>
                    <h2 className={style["chamada__texto__titulo"]}>Aproveite!</h2>
                    <p className={style["chamada__texto__subtitulo"]}>Descontos<br />progressivos</p>
                    <p className={style["chamada__texto__texto"]}>Na compra de 2 ou mais<br />livros distintos</p>
                    <a href="#" className={style["chamada__texto__link"]} onClick={() => setModalAtivo(true)}>Confira o regulamento</a>
                    <a href="#" className={style["chamada__texto__link--mobile"]} onClick={() => setModalAtivo(true)}>Saiba mais</a>
                </div>
            </div>
        </section>
    )
}