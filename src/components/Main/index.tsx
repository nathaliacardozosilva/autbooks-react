import Modal from "components/Modal";
import Chamada from "./Chamada";
import style from './Main.module.scss';
import Produto from "./Produto";
import { useState } from 'react';
import classNames from "classnames";
import Snackbar from "./Snackbar";

export interface ILivro {
    id: number;
    imagem: string;
    nome: string;
    valorInicial: number;
    valorTotal: number;
    quantidade: number;
}

interface Props {
    carrinho: boolean,
    setCarrinho: React.Dispatch<React.SetStateAction<boolean>>
}

export default function Main({ carrinho, setCarrinho }: Props) {
    const [modalAtivo, setModalAtivo] = useState(false);
    const [snackbar, setSnackbar] = useState(false);

    return (
        <>
            <Chamada modalAtivo={modalAtivo} setModalAtivo={setModalAtivo} />
            <div className={classNames({
                [style.fundoEscuro]: true,
                [style.fundoEscuroAtivo]: modalAtivo === true
            })}></div>
            <section className={classNames({
                [style.regulamento]: true,
                [style.regulamentoAtivo]: modalAtivo === true
            })}>
                <Modal setModalAtivo={setModalAtivo} />
            </section>
            <section className={style["produtos"]} id="produtos">
                <Produto snackbar={snackbar} setSnackbar={setSnackbar} />
            </section>
            <Snackbar setCarrinho={setCarrinho} snackbar={snackbar} setSnackbar={setSnackbar} />
        </>
    )
}
