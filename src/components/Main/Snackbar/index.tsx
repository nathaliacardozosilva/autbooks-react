import style from './Snackbar.module.scss';
import classNames from 'classnames';
import { useEffect } from 'react';

interface Props {
    snackbar: boolean,
    setSnackbar: React.Dispatch<React.SetStateAction<boolean>>,
    setCarrinho: React.Dispatch<React.SetStateAction<boolean>>
}

export default function Snackbar({ snackbar, setSnackbar, setCarrinho }: Props) {
    
    useEffect(() => {
        if (snackbar === true) {
            setTimeout(() => setSnackbar(false), 2000);
        }
    }, [snackbar])

    return (
        <section className={classNames({
            [style.snackbar]: true,
            [style.snackbarAtivo]: snackbar === true
        })}>
            <div className={style["snackbar__conteudo"]}>
                <p className={classNames({
                    [style.snackbar__texto]: true,
                    [style.texto]: true,
                })}>Produto adicionado ao carrinho</p>
                <a 
                    href="#" 
                    onClick={() => {
                        setSnackbar(false);
                        setCarrinho(true);
                    }} 
                    className={style["snackbar__link"]}
                >
                    Ir para o carrinho
                </a>
            </div>
        </section>

    )
}